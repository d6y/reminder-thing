name := "reminder-thing"
version := "3.0.0"
organization := "com.spiralarm"
scalaVersion := "2.12.1"

libraryDependencies ++= Seq(
  "com.spinoco" %% "fs2-http" % "0.1.6"
)

scalacOptions := Seq(
    "-deprecation",
    "-encoding", "UTF-8",
    "-unchecked",
    "-feature",
    "-language:implicitConversions",
    "-language:postfixOps",
    "-Ywarn-dead-code",
    "-Ywarn-value-discard",
    "-Xlint",
    "-Xfatal-warnings"
)

mainClass := Some("rt.Main")

