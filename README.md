# Reminder Thing

Every week this site goes to your Google Contacts and Calendar.
It looks forward two weeks, and emails you a summary of upcoming annual events.

# Structure

## Interface

- HTML and CSS front end to sign up and unsubscribe
- A JSON API for the front end to add and remove users.
- A Scala.js sits between the web front-end and the API.

## Backend

- Postgress database
- Schedule job to check weekly for each user
- Email system to send out records



