package rt

import fs2._
import spinoco.fs2.http
import http._
import http.body._
import spinoco.protocol.http.header._
import spinoco.protocol.http._
import spinoco.protocol.http.header.value._

import spinoco.fs2.http.routing._
import shapeless.{HNil, ::}

import java.net.InetSocketAddress
import java.util.concurrent.Executors
import java.nio.channels.AsynchronousChannelGroup

import java.nio.file.Paths

object Main {

  val ES = Executors.newCachedThreadPool(Strategy.daemonThreadFactory("ACG"))
  implicit val ACG = AsynchronousChannelGroup.withThreadPool(ES) // http.server requires an ACG
  implicit val S = Strategy.fromExecutor(ES) // Async (Task) requires a strategy

  def echoService(request: HttpRequestHeader, body: Stream[Task,Byte]): Stream[Task,HttpResponse[Task]] = {
    if (request.path != Uri.Path / "echo") Stream.emit(HttpResponse(HttpStatusCode.Ok).withUtf8Body("Hello World"))
    else {
      val ct =  request.headers.collectFirst { case `Content-Type`(ct) => ct }.getOrElse(ContentType(MediaType.`application/octet-stream`, None, None))
      val size = request.headers.collectFirst { case `Content-Length`(sz) => sz }.getOrElse(0l)
      val ok = HttpResponse(HttpStatusCode.Ok).chunkedEncoding.withContentType(ct).withBodySize(size)

      Stream.emit(ok.copy(body = body.take(size)))
    }
  }

  def fileService(request: HttpRequestHeader, body: Stream[Task,Byte]): Stream[Task,HttpResponse[Task]] = {
    val p = Paths.get("src/main/website/index.html")
    val len = p.toFile.length
    println(s"Len is $len")
    val html = ContentType(MediaType.`text/html`, Some(HttpCharset.`UTF-8`), boundary = None)
    import fs2.io
    if (request.path == Uri.Path / "index.html") {
      val ok = HttpResponse[Task](HttpStatusCode.Ok).withBodySize(len).withContentType(html)
      val read: Stream[Task, Byte] = io.file.readAll[Task](p, 1024*8)
      Stream.emit(ok.copy(body = read))
    }
    else Stream.empty//emit(HttpResponse(HttpStatusCode.NotFound))
  }


  def main(args: Array[String]): Unit = {
    val socket = new InetSocketAddress("127.0.0.1", 9090)
    http.server(socket)(fileService).run.unsafeRun()
  }
}
